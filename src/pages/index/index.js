import Taro, { Component } from '@tarojs/taro'
import { View, Button, Text } from '@tarojs/components'
import { connect } from '@tarojs/redux'
import { AtGrid } from 'taro-ui'
import { add, minus, asyncAdd } from '../../actions/counter'

import './index.scss'
const namespace = 'index';

// const mapStateToProps = (state) => {
//   const indexList = state[namespace].state;
//   console.log(state[namespace])
//   return {
//     indexList
//   };
// };
// console.log(`${namespace}`)
// const mapDispatchToProps = dispatch => ({
//   getMenu: payload => dispatch({ type: `${namespace}/getMenu`, payload }),
// });

// @connect(mapStateToProps, mapDispatchToProps)
export default class Index extends Component {

  config = {
    navigationBarTitleText: '首页'
  }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { 
    wx.getSetting({
      success(res) {
        if(res){
          console.log(res.authSetting)
        }
      }
    })
    wx.setBackgroundColor({
      backgroundColor: "linear-gradient(to bottom right, red , blue)" // 窗口的背景色为白色
    })
    
    wx.getUserInfo({
      success(res) {
        const userInfo = res.userInfo
        const nickName = userInfo.nickName
        const avatarUrl = userInfo.avatarUrl
        const gender = userInfo.gender // 性别 0：未知、1：男、2：女
        const province = userInfo.province
        const city = userInfo.city
        const country = userInfo.country
      }
    })
  }

  componentDidHide () { }

  onMenuClick(item,index){
    switch (index){
      case 0:
      Taro.navigateTo({url:'/pages/calendar/calendar'})
    }
  }
  render () {
    return (
      <View className='index'>
        <View className='index_menu'>
          <View className='main'>
            <View className='calendar'>
              <AtGrid columnNum='2' onClick={this.onMenuClick} data={
                [
                  {
                  iconInfo : {size:80, color: '#6FB4EC', value: 'calendar'},
                  // value: '排班日历'
                  },
                  {
                    iconInfo: { size: 80, color: '#F4D95B', value: 'money' },
                  // value: '找折扣'
                  },
                  {
                    iconInfo: { size: 80, color: '#F3456D', value: 'heart' },
                    // value: '领会员'
                  },
                  {
                    iconInfo: { size: 80, color: '#95B55B', value: 'star' },
                        // value: '新品首发'
                  },
                  {
                    iconInfo: { size: 80, color: '#F49930', value: 'bullet-list' },
                        // value: '领京豆'
                  },
                  {
                    iconInfo: { size: 80, color: '#F26D26', value: 'help' },
                        // value: '手机馆'
                  }
                ]
              } />
            </View>
          </View>
        </View>
      </View>
    )
  }
}

