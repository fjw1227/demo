import { routerRedux } from 'dva/router';

export default {
  namespace: 'index',
  state: {
    menulist:[]
  },

  effects: {
    *getMenu({payload},{call,put}){
        console.log(payload)
        yield put({
            type: 'menuList',
            payload: payload,
          });
        // switch (payload.index){
        //     case 0:
        //     console.log()
        }
    },
  reducers: {
    menuList(state, action) {
        return {
          ...state,
          list: action.payload,
        };
  },
}
};
