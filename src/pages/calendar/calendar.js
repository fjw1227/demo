import Taro, { Component } from '@tarojs/taro'
import { View, Button, Text } from '@tarojs/components'
import { connect } from '@tarojs/redux'
import { AtGrid, AtIcon , AtCalendar , AtButton} from 'taro-ui'
import { add, minus, asyncAdd } from '../../actions/counter'

import './index.scss'
const namespace = 'calendar';

// const mapStateToProps = (state) => {
//   const indexList = state[namespace].state;
//   console.log(state[namespace])
//   return {
//     indexList
//   };
// };
// console.log(`${namespace}`)
// const mapDispatchToProps = dispatch => ({
//   getMenu: payload => dispatch({ type: `${namespace}/getMenu`, payload }),
// });

// @connect(mapStateToProps, mapDispatchToProps)
export default class Calendar extends Component {

  config = {
    navigationBarTitleText: '排班日历',
    usingComponents: {
        'calendar': '../../components/calendar/calendar' // 书写第三方组件的相对路径
      }
  }
  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { 

  }

  componentDidHide () { }

  onMenuClick(item,index){

  }
  render () {
    const daysColor = [
        {
        
        }]
    return (
      <View className='index'>
        <AtIcon value='arrow-left' size='30' color='#333' className='backIcon'></AtIcon>
        <AtIcon value='bullet-list' size='30' color='#333' className='listIcon'></AtIcon>
         <View className='index_menu'>
          <View className='main'>
            <View className='calendar'>
                <calendar lunar='true'></calendar>
                <AtButton className='setBottom' circle='true' >设置排班</AtButton>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

